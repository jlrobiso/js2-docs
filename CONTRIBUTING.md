# Contributing to Jetstream2 Documentation

Thank you for considering contributing to the Jetstream2 documentation site! We welcome contributions from the community to help improve and maintain our documentation.

## How to Contribute

Contributions follow a typical git-based workflow:

1. [Fork the repository](https://gitlab.com/jetstream-cloud/docs/-/forks/new)
2. Clone your new repository
3. Make changes
4. Commit/Push changes to your fork
5. Submit a merge request to the upstream project with your fork as the "source" and the `main` branch of `jetstream-cloud/docs` as the "target." Be sure to include a detailed description of the changes you've made.

## Guidelines

When contributing, please adhere to the following basic guidelines:

- Ensure all content added is relevant, accurate, and well-written.
- Try to follow the existing style and formatting guidelines (see below).
- Preview your changes to ensure they display correctly.
- Provide descriptive commit messages for clarity.
- Avoid breaking changes to existing links and directory structures wherever possible.

## Building Locally

### Prerequisites

To build the docs locally, you'll first need [Python 3](https://docs.python.org/3/using/index.html) and `pip` installed.

After cloning the repository, `cd` into it and grab the listed dependencies:
```bash
python3 -m pip install -r ./requirements.txt
```

### Building

```bash
python3 -m mkdocs build

# OR, if the default pip installation location is in your $PATH
mkdocs build
```

The generated HTML will be built in a folder called `site`. Passing the `--clean` flag may also help with certain caching issues.

### Serving

You can easily preview changes on your local using MkDocs:
```bash
python3 -m mkdocs serve

# OR, if the default pip installation location is in your $PATH
mkdocs serve
```
Open a web browser and connect to the server by navigating to `http://localhost:8000`.

**Note**: While this server is running, changes to the docs files are automatically rebuilt and browser tabs refreshed. Making changes to certain files (like `mkdocs.yml`), though, may require you to restart the server.

## Previewing Changes with GitLab Artifacts

If building/previewing the docs locally isn't your cup of tea, you're in luck! After every push to GitLab, a CI/CD pipeline will run to build your changes and save them as an artifact. In fact, changes to the `main` branch on your fork are deployed to `https://<your-gitlab-username>.gitlab.io/docs` automagically when the pipeline finishes.

For other branches, you can preview changes using GitLab's browsable artifacts:

1. On GitLab's left sidebar, go to `Operate` → `Environments`

    ![screenshot of navigation pane on gitlab.com with arrows pointing to "Operate" and "Environments"](https://i.imgur.com/q3pB0Yl.png)

2. Find the deployment for your branch and click "Open"

    ![screenshot of environment on gitlab.com with mouse hovering over "Open" button](https://i.imgur.com/VsgUC0F.png)

## Style and Formatting Guidelines

**Note**: If you're a new contributor, don't worry about getting every detail perfect! We'll work with you to polish it during review.

### General Writing Guidelines

#### Language and Tone

- Use clear and concise language
- Write in active voice for directness and clarity
- Expand acronyms the first time you use them. For example, "The OpenStack Command-Line Interface (CLI)..."
- If using jargon is necessary, consider providing context to uninformed users via links to relevant definitions/explanations, `abbr` hovers, etc.
- Avoid using the words [ACCESS](https://access-ci.org) and "access" near each other.

#### Grammar and Spelling

- Proofread all content for grammar and spelling errors.
- Use a spell checker if available.

#### Examples and Illustrations

- Provide relevant examples to clarify concepts.
- Use diagrams or illustrations where necessary to aid understanding.

### Markdown Usage

Basic information about Markdown syntax can be found online -- [markdownguide.org](https://www.markdownguide.org/basic-syntax/) is a particularly good resource. (Note that mkdocs uses [Python-Markdown](https://github.com/Python-Markdown/markdown), which may differ slightly from other implementations and the [CommonMark](https://commonmark.org) spec.)

#### Headings

Use consistent heading levels to organize content logically.

- `#` should be reserved for the overall document title.
- `##` for major sections
- `###` for subsections
- etc.

If you make it past `###`, consider breaking out some information into its own separate document! 

#### Links

- Use descriptive link text for clarity.
- Never use absolute or HTML paths for internal links. Prefer to link to raw Markdown files *relative to the current doc's directory*.

    |      | Example (for `docs/ui/exo/example.md`) |
    |-----|-----------------------------------------|
    | ✅ | `[link](exo.md)`                        |
    | ✅ | `[link](../horizon/intro.md)`           |
    | ✅ | `[link](../../general/docker.md)`       |
    | ❌ | `[link](../../../general/docker)`       |
    | ❌ | `[link](/general/docker.md)`            |

- Ensure all links are working and point to the correct destinations.
- Try to stick to trusted, well-known sources for external resources. Otherwise, consider linking to a snapshot on the [Wayback Machine](http://web.archive.org/) instead.
- External links should be tagged with `[](){target=_blank;}` so that they open in a new tab in the reader's browser.

#### Images

- Include descriptive alt text for images.
- Never use a raw `<img>` tag instead of the Markdown `![alt](src)` syntax.
- Images should be stored in the `docs/images` folder, **not** linked from externally.

#### Raw HTML/CSS/JavaScipt

While it is possible to insert raw HTML into MkDocs Markdown files, you should **always** prefer to use Markdown-specific syntax whenever possible. If you have to resort to raw HTML, consider investigating whether any Markdown/MkDocs extensions or themes are available to accomplish the same result instead.

## Testing Links with Lychee

[Lychee](https://lychee.cli.rs/) is a Rust-based tool used to check links for validity, both internal and external. It is run automatically in the `test` stage of GitLab's CI/CD pipeline(s) and helps us to identify broken links on our site before users do.

**Note** that a GitLab pipeline will fail if Lychee does not pass.

### Running Lychee Locally

If you want to run Lychee against your local changes (instead of in GitLab CI), you'll need to install it. See [their GitHub README](https://github.com/lycheeverse/lychee?tab=readme-ov-file#installation) for instructions for various distros.

Be sure to include the `lychee.toml` config file and run it against the `docs` folder:

```bash
lychee --config ./lychee.toml ./docs
```

Alternatively, it can be used through Docker:
```bash
# Run from this repository's root folder
docker run --init -it --rm -w /input -v $(pwd):/input lycheeverse/lychee --config ./lychee.toml  ./docs
```

## Updating the Usage Estimation Calculator
The usage estimator calculator (located at `docs/alloc/estimator.md`) is a web component written in Vue 3 and TypeScript, and source code can be found at https://github.com/kjunuh/js2-usage-estimator. 

To update the estimator, follow the "project setup" found at https://github.com/kjunuh/js2-usage-estimator#project-setup. After making edits and building, copy and rename the resulting javascript file found at `usage-estimator/dist/assets/index.{build}.js` into `docs/js/usage_estimator.js`. After mkdocs rebuilds, the estimator page should load using the updated calculator web component.


## Feedback and Support

If you have any questions, encounter issues, or need further assistance, please don't hesitate to [open an issue](https://gitlab.com/jetstream-cloud/docs/-/issues/new) or reach out to us via help@jetstream-cloud.org.
